const connection = require("../config/db");

const getProduct = (req, res) => {
connection.query("SELECT * FROM produits", (err, rows) => {
    if (err) throw err;

    res.json(rows);
});
};

const getProductById = (req, res) => {
    connection.query("SELECT * FROM produits WHERE id_produit = ?",[req.params.id_produit], (err, rows) => {
        if (err) throw err;
    
        res.json(rows[0]);
    });
};

const createProduct = (req, res) => {
    connection.query('INSERT INTO produits SET nom_produit=? ,prix=?,id_client=?', [req.body.nom_produit,req.body.prix,req.body.id_client],(err, rows) =>{
        if (err) throw err;
        res.json("le produit a été bien enregistré");
    });
      
};

const deleteProduct = (req, res) => {
    connection.query("DELETE FROM produits WHERE id_produit = ?",[req.params.id_produit], (err, rows) => {
        if (err) throw err;
    
        res.json("le produit a été supprimé avec succes");
    });
};


module.exports={getProduct,getProductById,createProduct,deleteProduct};