const connection = require("../config/db");

const getUsers = (req, res) => {
connection.query("SELECT * FROM user", (err, rows) => {
    if (err) throw err;

    res.json(rows);
});
};

const getUserById = (req, res) => {
    connection.query("SELECT * FROM user WHERE id = ?",[req.params.id], (err, rows) => {
        if (err) throw err;
    
        res.json(rows[0]);
    });
};

const createUser = (req, res) => {
    connection.query('INSERT INTO user SET nom=? ,addresse=?,password=?,contact=?', [req.body.nom,req.body.addresse,req.body.password,req.body.contact],(err, rows) =>{
        if (err) throw err;
        res.json("l'utilisateur a été bien enregistré");
    });
      
};

const deleteUser = (req, res) => {
    connection.query("DELETE FROM user WHERE id = ?",[req.params.id], (err, rows) => {
        if (err) throw err;
    
        res.json("utilisateur supprimé avec succes");
    });
};


module.exports={getUsers,getUserById,createUser,deleteUser};